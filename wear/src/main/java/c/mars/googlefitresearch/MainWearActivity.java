package c.mars.googlefitresearch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.wearable.view.WatchViewStub;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import c.mars.fitlib.Client;
import c.mars.fitlib.Sensors;
import c.mars.fitlib.common.Display;
import timber.log.Timber;

public class MainWearActivity extends Activity {

    @InjectView(R.id.text)
    TextView textView;

    private Client client;
    private Sensors sensors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Timber.plant(new Timber.DebugTree());

        final Activity activity = this;
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                ButterKnife.inject(activity);

                final Handler displayHandler = new DisplayHandler(textView, Looper.getMainLooper());

                final Display display = new Display(MainWearActivity.class.getSimpleName()) {
                    @Override
                    public void show(String msg) {
                        Message completeMessage = displayHandler.obtainMessage(0, msg);
                        completeMessage.sendToTarget();
                        Timber.d(msg);
                    }
                };

                client = new Client(activity,
                        new Client.Connection() {
                            @Override
                            public void onConnected() {
                                sensors = new Sensors(client.getClient(),
                                        new Sensors.DatasourcesListener() {
                                            @Override
                                            public void onDatasourcesListed() {
                                                display.show("datasources:");
                                                ArrayList<String> datasources = sensors.getDatasources();
                                                for (String d:datasources) {
                                                    display.show(d);
                                                }
                                            }
                                        },
                                display);
                                sensors.subscribeToHeartRate();
                            }
                        },
                        display);

                client.connect();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        client.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        sensors.unsubscribe();
        client.disconnect();
        super.onStop();
    }

    private static class DisplayHandler extends Handler {
        public DisplayHandler(TextView textView, Looper looper) {
            super(looper);
            this.textView = textView;
        }

        private TextView textView;

        @Override
        public void handleMessage(Message msg) {
            String msgTxt = (String)msg.obj;
            Timber.d(msgTxt);
            textView.append("\n"+msgTxt);
        }
    }
}
